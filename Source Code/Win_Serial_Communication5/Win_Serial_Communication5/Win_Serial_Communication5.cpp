// Win_Serial_Communication5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <commdlg.h>

int nread, nwrite;
DCB dcb;

HANDLE open_serial_port(const char* device, uint32_t baudrate)
{
	HANDLE hcom = CreateFileA(device,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hcom == INVALID_HANDLE_VALUE)
	{
		printf("CreateFile failed with error %d. \n", GetLastError());
		return INVALID_HANDLE_VALUE;
	}

	BOOL success1 = FlushFileBuffers(hcom);
	if (!success1)
	{
		printf("Faild to flush serial port %d. \n", GetLastError());
		CloseHandle(hcom);
		return INVALID_HANDLE_VALUE;
	}

	COMMTIMEOUTS timeouts = { 0 };
	timeouts.ReadIntervalTimeout = 10;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 5000;
	timeouts.WriteTotalTimeoutMultiplier = 0;

	BOOL success2 = SetCommTimeouts(hcom, &timeouts);
	if (!success2)
	{
		printf("Failed to set seial timeouts %d. \n", GetLastError());
		CloseHandle(hcom);
		return INVALID_HANDLE_VALUE;
	}


	dcb.DCBlength = sizeof(DCB);
	dcb.BaudRate = baudrate;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	BOOL success3 = SetCommState(hcom, &dcb);
	if (!success3)
	{
		printf("Failed to set serial settings %d. \n", GetLastError());
		CloseHandle(hcom);
		return INVALID_HANDLE_VALUE;
	}

	return hcom;
}

int main()
{
	char send[32];
	char send2[1000];
	char receive[1000];
	DWORD rwlen = 0;
	char *ptrBufferWrite,*ptrBufferWrite2, *ptrBufferRead;
	const char *device = "COM9";
	uint32_t baudrate = CBR_115200;
	HANDLE port = open_serial_port(device, baudrate);
	if (port == INVALID_HANDLE_VALUE)
		return 1;
	printf("Serial port %s successfully reconfigured. \n", device);

	
	//ptrBufferRead = NULL;
	while (1)
	{

		printf("Enter a string to send via serial port \n");
		memset(send, 0, 32);
		memset(send2, 0, 1000);
		for (int i = 0; i < 31; i++)
		{
			send[i] = getchar();
			if (send[i - 1] == '"' && send[i] == '\n')
			{
				send[i] = '\r\n';
				for (int j = 0; j < 1000; j++)
				{
					send2[j] = getchar();
					if (send2[j] == '\x1a')
					{
						break;
					}

				}

			}
			if (send[i] == '\n')
			{
				send[i] = '\r\n';
				break;
			}
		}

		printf("The send command is %s with the size of %d bytes\n", send, strlen(send));
		ptrBufferWrite = &send[0];
		ptrBufferWrite2 = &send2[0];
		if (!WriteFile(port, ptrBufferWrite, sizeof(send), &rwlen, NULL))
		{
			printf("error writing to uotput buffer \n");
		}
		if (!WriteFile(port, ptrBufferWrite2, sizeof(send2), &rwlen, NULL))
		{
			printf("error writing to uotput buffer SMS \n");
		}
		printf("Data written to write buffer is \n %s \n", ptrBufferWrite);
		printf("Number of Bytes written is \n %d \n", rwlen);

		/*if (ptrBufferWrite =="AT+CMGS=\"0906345443\"")
		{
			for (int x = 0; x < 1000; x++)
			{
				send2[x] = getchar();
				if (send2[x] == '^Z')
				{
					send2[x + 1] = '\r\n';
					break;
				}
			}
		}*/

		ptrBufferRead = (char *)calloc(1000, 1);
		//memset(receive, 32, 0);
		//ptrBufferRead = &receive[0];
		rwlen = 0;
		if (!ReadFile(port, ptrBufferRead, 1000, &rwlen, NULL))
		{
			printf("error reading from input buffer \n");
		}


		printf("Data read from read buffer is \n %s \n", ptrBufferRead);
		memset(ptrBufferRead, 0, 1000);
		free(ptrBufferRead);

		if (ptrBufferWrite  == "end\n")
			break;
	}
	

	CloseHandle(port);
	getchar();


}

