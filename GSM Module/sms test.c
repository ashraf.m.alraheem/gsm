#include <SoftwareSerial.h> 	

SoftwareSerial mySerial(3, 2)	//SIM800L Tx & Rx is connected to Arduino #3 & #2

void setup()
{
	Serial.begin(9600);		//Begin serial communication with Arduino and Arduino IDE (Serial Monitor)
	mySerial.begin(9600);		//Begin serial communication with Arduino and SIM800L
	Serial.println("Initializing...");
	delay(1000);
	
	mySerial.println("AT");		//Once the handshake test is successful, it will back to OK
	updateSerial();
	
	mySerial.println("AT+CMGF=1");		//Configuring TEXT mode
	updateSerial();
	mySerial.println("AT+CMGS=\"+249906345443\"");
	updateSerial();
	mySerial.println("Alaa Abdel Moneim");
	updateSerial();
	mySerial.write(26);
	
}

void loop()
{
}

void updateSerial()
{
	delay(500);
	while (serial.available())
	{
		mySerial.write(Serial.read());		//Forward what Serial received to Software Serial Port
	}
	while (mySerial.available())
	{
		Serial.write(mySerial.read());		//Forward what Software Serial received to Serial Port
	}
}